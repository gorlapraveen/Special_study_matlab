clc;
close all;

t=192e-6;
N=192;
fd=11e6;
[Z12]=z12ambiguityfunction;
[Z23]=z23ambiguityfunction;
za=0.5.*(real(Z12+Z23));
display(za);
pa=length(za);
%for i=1:p
    pb=real(sinc(pi.*fd*11*t.*N));
    if length(za)>length(pb)
        pc=length(za)-length(pb);
        pb=[pb, zeros(1,pc)];
    else
       pc=length(pb)-length(za);
       za=[za,zeros(1,pc)];
    end
        
    xa=pb*za;
    display(xa);
    xb = ambiguityILL(xa, xa, 'true', fd);
    ambigsize=size(xb);
    xadb=-10.*log10(xb);
%end
[x,y]=coordAxes(fd,ambigsize(1),'1',p);
mesh(x,y,xb)
hold on;
semilogy(x,xadb);
hold on
semilogy(y,xadb)