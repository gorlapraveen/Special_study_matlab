
clc;
close all;

%The distance between the Tx antenna (at 2.4GHz, -10dBm amp)to Rx is 30 cm.
%First the received values are observed when there is obstacle between Tx
%and Rx antennas.
% Certain values have been observed and avg is done
rx_no_obs = [-41.84 -41.43 -41.21 -41.26 -41.15 -41.42 -41.78];
len=length(rx_no_obs);
rx_no_obs_sum = -290.09;
rx_no_obs_avg =(rx_no_obs_sum)/len;
display(rx_no_obs_avg);
x=-15:3:45; % Generally obstacle distance is increased 3cm from start(-15cm) to end (-45cm)
rx_obs=[-43.3555 -43.231 -42.47 -44.9916 -42.706 -53.67 -47.09 -46.02 -46.365 -47.3416 -44.605 -44.61 -43.518333 -44.2966 -48.368 -51.4316 -42.511 -43.35 -45.7688 -43.475 -43.091];

display(rx_no_obs_avg);
figure;

semilogy(x,rx_no_obs_avg,'ks');
hold on
semilogy(x,rx_obs,'ks-');
axis([-20 +50 -54 -40]);
grid on;
legend('Avg Rx_ dBm when no obstracle is placed', 'Rx_dbm_received by receiver when obstracle moves from Tx to Rx');
xlabel('Distnce (in cm, 30 cm tx-Rx ditance) when Obstracle moves from Tx to Rx taken for every 3cm from -15cm to 45 cm')
ylabel('Received Rx_dBm ');
title('When obstracle is placed at different distances from Tx and Rx (LOS)');
hold off;
