%% Plot Autocorrelation Sequences of Rectangular and Linear FM Pulses
% This example shows how to plot zero-Doppler cuts of the
% autocorrelation sequences of rectangular and linear FM pulses
% of equal duration. Note the pulse compression exhibited in
% the autocorrelation sequence of the linear FM pulse.

% Copyright 2015 The MathWorks, Inc.


hrect = phased.RectangularWaveform('PRF',2e4);
hfm = phased.LinearFMWaveform('PRF',2e4);
xrect = step(hrect);
xfm = step(hfm);
[ambrect,delayrect] = ambgfun(xrect,hrect.SampleRate,...,
    hrect.PRF,'Cut','Doppler');
[ambfm,delayfm] = ambgfun(xfm,hfm.SampleRate,...,
    hfm.PRF,'Cut','Doppler');
figure;
subplot(211);
stem(delayrect,ambrect);
title('Autocorrelation of Rectangular Pulse');
subplot(212);
stem(delayfm,ambfm)
xlabel('Delay (seconds)');
title('Autocorrelation of Linear FM Pulse');