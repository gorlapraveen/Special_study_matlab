%% Plot Ambiguity Function of Rectangular Pulse
% Plot the ambiguity function magnitude of a rectangular pulse.
waveform = phased.RectangularWaveform;
x = waveform();
PRF = 2e4;
[afmag,delay,doppler] = ambgfun(x,waveform.SampleRate,PRF);
contour(delay,doppler,afmag)
xlabel('Delay (seconds)')
ylabel('Doppler Shift (hertz)')