
% Direct Sequence Spread Spectrum
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [qpsk_sig] = DSSS_DQPSK_Barker 
clc
clear
M=4;

% Generating the bit pattern with each bit 28 samples long

barker=[1 -1 1 1 -1 1 1 1 -1 -1 -1];
pattern_for_dss=[];
for k=1:11
    if barker(1,k)==-1
        signal=-ones(1,28);
    else
        signal=ones(1,28);
    end
    
    pattern_for_dss=[pattern_for_dss signal];
end
subplot(4,1,1)
plot(pattern_for_dss);
axis([-1 308 -1.5 1.5]);
title('Original barker Bit Sequence');


% Generating the pseudo random bit pattern for spreading
d=round(rand(1,121));
pn_seq=[];
carrier=[];
t=[0:2*pi/3:2*pi];     % Creating 5 samples for one cosine 
for k=1:77
    if d(1,k)==0
        signal=-ones(1,4);
    else
        signal=ones(1,4);
    end
   
    pn_seq=[pn_seq signal];
   
end

% Spreading of sequence
spreaded_sig=pattern_for_dss.*pn_seq;
subplot(4,1,2)
plot(spreaded_sig)
axis([-1 308 -1.5 1.5]);
title('Spreaded signal');
display(size(spreaded_sig));
display(size(carrier))

len =length(spreaded_sig); 
display(len);
for i=1:len
    if spreaded_sig(i)==-1
        spreaded_sig(i)=0.*spreaded_sig(i);
        
    else
        spreaded_sig(i)=1.*spreaded_sig(i);
         
        
    end
end

qpsk_sig=dpskmod(spreaded_sig,M,pi/4);
display(length(qpsk_sig));
subplot(4,1,3);

plot(qpsk_sig);
title('DQPSK modulated signal');
%Plotting the FFT of DSSS signal

y=abs(fft(xcorr(qpsk_sig)));
subplot(4,1,4)
plot(y/max(y))
xlabel('Frequency')
ylabel('PSD')
end

