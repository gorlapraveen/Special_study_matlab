clc;
close all;

%The distance between the Tx antenna (at 2.4GHz, -10dBm amp)to Rx is 30cm.
%First the recieved values are observed when there is obstracle between Tx
%and Rx antennas.
% Certain values have been observed and avg is done
rx_no_obs = [-41.84 -41.43 -41.21 -41.26 -41.15 -41.42 -41.78];
len=length(rx_no_obs);
rx_no_obs_sum = -290.09;
rx_no_obs_avg =(rx_no_obs_sum)/len;
display(rx_no_obs_avg);
x=0:3:30;
rx_obs=[-53.67 -47.09 -46.02 -46.365 -47.3416 -44.605 -44.61 -43.518333 -44.2966 -48.368 -51.4316];

display(rx_no_obs_avg);
figure;
semilogy(x,rx_no_obs_avg,'mx-');
hold on
semilogy(x,rx_obs,'bs-');
grid on
legend('Avg Rx_dBm_when no obstracle is placed','Rx_dbm_received by receiver when obstracle moves from Tx to Rx');
xlabel('Distnce (in cm, 30 cm tx-Rx ditance) when Obstracle moves from Tx to Rx taken for every 3cm from o to 30 cm')
