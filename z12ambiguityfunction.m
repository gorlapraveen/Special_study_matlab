function [Z12]=z12ambiguityfunction
Ut1=[-1 -1 1 -1 -1 1 -1 1 1 1 1];
Ut2=Ut1;
normalize='true';
fftPoint=11e6;
Z12 = ambiguityILL(Ut1, Ut2, normalize, fftPoint);
ambigSize=size(Z12);
pulseLength=length(Ut1);
[x,y]=coordAxes(fftPoint,ambigSize(2),'1',pulseLength);
display(Z12);
display(ambigSize);
mesh(x,y,Z12);
end
 