%SANJEET KUMAR
%M.Tech(Telecommunication) 
%NIT Calicut

% g(t) is a rectangular function
echo on
T=1;
M=4;
Es=T/2;
fc=6/T;
N=100;
dT=T/(N-1);
t=0:dT:T;
u0=sqrt(2*Es/T)*cos(2*pi*fc*t);
u1=sqrt(2*Es/T)*cos(2*pi*fc*t+2*pi/M);
u2=sqrt(2*Es/T)*cos(2*pi*fc*t+4*pi/M);
u3=sqrt(2*Es/T)*cos(2*pi*fc*t+6*pi/M);
subplot(5,1,1);
plot(t,u0);
title('QPSK Constant Envelope Signal Waveforms(Rectangular Pulse Shape)');
xlabel('time,t(s)-->');
ylabel('u0(t)-->');
subplot(5,1,2);
plot(t,u1);
xlabel('time,t(s)-->');
ylabel('u1(t)-->');
subplot(5,1,3);
plot(t,u2);
xlabel('time,t(s)-->');
ylabel('u2(t)-->');
subplot(5,1,4);
plot(t,u3);
xlabel('time,t(s)-->');
ylabel('u3(t)-->');
y=(abs(fft(u0))).*(abs(fft(u0)));
subplot(5,1,5);
plot(y);
title('POWER SPECTRAL DENSITY');
xlabel('frequency,f(hz)-->');
ylabel('u0(f) (watts/hz)-->');