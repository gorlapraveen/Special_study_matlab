%By SANJEET KUMAR 
%NIT CALICUT,M.Tech(Telecommunication)

% Direct Sequence Spread Spectrum
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc
clear

% Generating the bit pattern with each bit 20 samples long

b=[1 -1 1 1 -1 1 1 1 -1 -1 -1];
pattern=[];
for k=1:11
    if b(1,k)==-1
        sig=-ones(1,28);
    else
        sig=ones(1,28);
    end
    
    pattern=[pattern sig];
end
subplot(4,1,1)
plot(pattern);
axis([-1 320 -1.5 1.5]);
title('Original Bit Sequence');


% Generating the pseudo random bit pattern for spreading
d=round(rand(1,121));
pn_seq=[];
carrier=[];
t=[0:2*pi/3:2*pi];     % Creating 5 samples for one cosine 
for k=1:77
    if d(1,k)==-1
        sig=-ones(1,4);
    else
        sig=ones(1,4);
    end
    c=cos(t);   
    carrier=[carrier c];
    pn_seq=[pn_seq sig];
   
end

% Spreading of sequence
spreaded_sig=pattern.*pn_seq;
subplot(4,1,2)
plot(spreaded_sig)
axis([-1 320 -1.5 1.5]);
title('Spreaded signal');
display(size(spreaded_sig));
display(size(carrier));

% BPSK Modulation of the spreaded signal
bpsk_sig=spreaded_sig.*carrier;   % Modulating the signal
subplot(4,1,3);
plot(bpsk_sig)
axis([-1 320 -1.5 1.5]);
title('BPSK Modulated Signal');

%Plotting the FFT of DSSS signal

y=abs(fft(xcorr(bpsk_sig)));
subplot(4,1,4)
plot(y/max(y))
xlabel('Frequency')
ylabel('PSD')