
% Direct Sequence Spread Spectrum
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [dbpsk_sig] = DSSS_DBPSK_Barker 
clc
clear

% Generating the bit pattern with each bit 28 samples long

barker=[1 -1 1 1 -1 1 1 1 -1 -1 -1];
pattern_for_dss=[];
for k=1:11
    if barker(1,k)==-1
        signal=-ones(1,28);
    else
        signal=ones(1,28);
    end
    
    pattern_for_dss=[pattern_for_dss signal];
end
subplot(4,1,1)
plot(pattern_for_dss);
axis([-1 308 -1.5 1.5]);
title('Original barker Bit Sequence');


% Generating the pseudo random bit pattern for spreading
d=round(rand(1,121));
pn_seq=[];
carrier=[];
t=[0:2*pi/3:2*pi];     % Creating 5 samples for one cosine 
for k=1:77
    if d(1,k)==0
        signal=-ones(1,4);
    else
        signal=ones(1,4);
    end
    c=cos(t);   
    carrier=[carrier c];
    pn_seq=[pn_seq signal];
   
end

% Spreading of sequence
spreaded_sig=pattern_for_dss.*pn_seq;
subplot(4,1,2)
plot(spreaded_sig)
axis([-1 308 -1.5 1.5]);
title('Spreaded signal');
display(size(spreaded_sig));
display(size(carrier));

% BPSK Modulation of the spreaded signal
%dbpsk_sig=spreaded_sig.*carrier;   % Modulating the signal
dbpsk_sig=mod(filter(1,[1 -1],spreaded_sig),2); %differential encoding y[n]=y[n-1]=x[n]
dbpsk_sig=dbpsk_sig.*carrier;
subplot(4,1,3);
plot(dbpsk_sig)
axis([-1 308 -1.5 1.5]);
title('DBPSK Modulated Signal');
disp(size(dbpsk_sig));

%Plotting the FFT of DSSS signal

y=abs(fft(xcorr(dbpsk_sig)));
subplot(4,1,4)
plot(y/max(y))
xlabel('Frequency')
ylabel('PSD')