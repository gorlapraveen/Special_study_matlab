
clc;
close all;

%The distance between the Tx antenna (at 2.4GHz, -10dBm amp)to Rx is 30 cm.
%First the received values are observed when there is obstacle between Tx
%and Rx antennas.
% Certain values have been observed and avg is done
rx_no_obs1 = [-41.84 -41.43 -41.21 -41.26 -41.15 -41.42 -41.78];
rx_no_obs2 = [-43.70 -43.30 -43.07 -43.92 -43.27 -44.40 -43.60 -43.83 -43.76 -44.15 -43.88];

rx_no_obs3 =[-44.59 -43.57 -44.99 -43.64 -44.34 -45.04 -45.21 -43.61 -44.05 -43.89];
len1=length(rx_no_obs1);
len2=length(rx_no_obs2);
len3=length(rx_no_obs3);
rx_no_obs1_sum = -290.09;
rx_no_obs2_sum =-480.58;  
rx_no_obs3_sum =-442.92;
rx_no_obs1_avg =(rx_no_obs1_sum)/len1;
rx_no_obs2_avg =(rx_no_obs2_sum)/len2;
rx_no_obs3_avg =(rx_no_obs3_sum)/len3;
display(rx_no_obs1_avg);
display(rx_no_obs2_avg);
display(rx_no_obs3_avg);
x=-15:3:45; % Generally obstacle distance is increased 3cm from start(-15cm) to end (-45cm)
rx_obs1=[-43.3555 -43.231 -42.47 -44.9916 -42.706 -53.67 -47.09 -46.02 -46.365 -47.3416 -44.605 -44.61 -43.518333 -44.2966 -48.368 -51.4316 -42.511 -43.35 -45.7688 -43.475 -43.091];
rx_obs2=[-45.22666 -44.7869 -46.1066 -44.9266 -45.6616 -55.1383 -50.74166 -49.7233 -47.6116 -50.01833 -48.07 -50.2833 -49.3 -51.87 -53.0133 -55.3583 -47.39 -44.9666 -46.39 -44.6083 -45.6833];
rx_obs3=[-45.4116 -43.3166 -44.705 -43.41833 -46.9916 -51.1183 -47.2561 -49.45 -46.425 -47.745 -46.435 -46.4883 -46.8266 -47.5916 -49.21833 -50.4533 -47.39 -44.9666 -46.44833 -44.61666 -45.66166];

figure;

semilogy(x,rx_no_obs1_avg,'ks');
hold on;
semilogy(x,rx_obs1,'o-');
axis([-20 +50 -54 -40]);
grid on;
legend('Avg Rx_ dBm when no obstracle is placed', 'Rx_dbm_received by receiver when obstracle moves from Tx to Rx');
xlabel('Distnce (in cm, 30 cm tx-Rx ditance) when Obstracle moves from Tx to Rx taken for every 3cm from -15cm to 45 cm')
ylabel('Received Rx_dBm ');
title('1st experiments - When obstracle is placed at different distances from Tx and Rx (LOS)');

figure;
hold on;
semilogy(x,rx_no_obs2_avg,'ks');
semilogy(x,rx_obs2,'o-');
axis([-20 +50 -54 -40]);
grid on;
legend('Avg Rx_ dBm when no obstracle is placed','Rx_dbm_received by receiver when obstracle moves from Tx to Rx')
xlabel('Distnce (in cm, 30 cm tx-Rx ditance) when Obstracle moves from Tx to Rx taken for every 3cm from -15cm to 45 cm')
ylabel('Received Rx_dBm ');
title('2nd time experiment is conducted - When obstracle is placed at different distances from Tx and Rx (LOS)');

figure;
semilogy(x,rx_no_obs3_avg,'ks');
hold on;
semilogy(x,rx_obs3,'o-');
axis([-20 +50 -54 -40]);
grid on;

legend('Avg Rx_ dBm when no obstracle is placed','Rx_dbm_received by receiver when obstracle moves from Tx to Rx');
xlabel('Distnce (in cm, 30 cm tx-Rx ditance) when Obstracle moves from Tx to Rx taken for every 3cm from -15cm to 45 cm')
ylabel('Received Rx_dBm ');
title('3rd time experiment is conducted - When obstracle is placed at different distances from Tx and Rx (LOS)');
hold off;
