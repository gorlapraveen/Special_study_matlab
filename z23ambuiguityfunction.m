function [z23]=z23ambuiguityfunction
Ut1=[1 -1 -1 -1 1 1 1 1 -1 1 -1 ];
Ut2=Ut1;
normalize='true';
fftPoint=256;
Z23 = ambiguityILL(Ut1, Ut2, normalize, fftPoint);
ambigSize=size(Z23);

pulseLength=length(Ut1);
[x,y]=coordAxes(fftPoint,ambigSize(2),'1',pulseLength);
display(Z23);
display(ambigSize);
mesh(x,y,Z23);
end

